@demoblaze
Feature: Login to homepage

    Scenario: as a user, i can login to homepage

    Given user is on the demoblaze login page
    When user is successfully logged in to demoblaze
    Then user should see demoblaze homepage