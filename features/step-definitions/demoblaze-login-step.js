import { Given, When, Then } from '@cucumber/cucumber';
import LoginPage from '../pageobjects/demoblaze/demoblaze-login-page'

Given(/^user is on the demoblaze login page$/, () => {
    LoginPage.open();
});

When(/^user is successfully logged in to demoblaze$/, () => {
    LoginPage.clickLinkTextLogin();
    LoginPage.inputUsername('rfirda');
    LoginPage.inputPassword('rfirda');
    LoginPage.clickBtnLogin();
});